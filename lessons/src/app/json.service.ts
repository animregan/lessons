import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';

@Injectable({
    providedIn: 'root'
})
export class JsonService {
    constructor(private http: HttpClient) {
    }

    public getJson(_path: string) : string {
        var jsonData = "";
        
        this.readJson(_path).subscribe(data => {
            jsonData = data;
            console.log(jsonData);
        });

        return jsonData;
    }

    public readJson(_path: string): Observable<any> {
        return this.http.get(_path)
    }
}