import { NgModule } from '@angular/core';
import { Title } from '@angular/platform-browser';
import * as $ from 'jquery';
import { TimeService } from './time.service';
import { JsonService } from './json.service';

@NgModule({
    providers: [
        TimeService,
        JsonService
    ]
})
// @Injectable()
export class GlobalsService {
    constructor(private titleService: Title, _timeService: TimeService, _jsonService: JsonService) {
        this.timeService = _timeService;
        this.jsonService = _jsonService;
    }
    
    title: string = "test";
    timeService : TimeService;
    jsonService: JsonService;

    SetTitle(_title: string) {
        this.title = _title;
        this.titleService.setTitle(_title);
    }

    SetPositionByNav() {
        $('#content-list').css("top", $('nav').css("height"));
        $('#content').css("top", $('nav').css("height"));
    }
}
