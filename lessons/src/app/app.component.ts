import { Component } from '@angular/core';

import { Title } from '@angular/platform-browser';
import { GlobalsService } from './globals.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public constructor(private titleService: Title, private globals: GlobalsService) { }
}
