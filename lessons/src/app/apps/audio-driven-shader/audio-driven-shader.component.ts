import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../globals.service';

@Component({
    selector: 'app-audio-driven-shader',
    templateUrl: './audio-driven-shader.component.html',
    styleUrls: ['./audio-driven-shader.component.css']
})
export class AudioDrivenShaderComponent implements OnInit {
    constructor(private _globals: GlobalsService) {
        _globals.SetTitle("Audio Driven Shader");
    }

    ngOnInit() {
    }
}
