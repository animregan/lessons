import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AppsComponent } from './apps.component';
import { AppListComponent } from './app-list/app-list.component';
import { GetTheCakeComponent } from './get-the-cake/get-the-cake.component';
import { AnAllHallowsTaleComponent } from './an-all-hallows-tale/an-all-hallows-tale.component';
import { TestComponent } from './test/test.component';
import { AudioDrivenShaderComponent } from './audio-driven-shader/audio-driven-shader.component';
import { SoundbathComponent } from './soundbath/soundbath.component';
import { SounbathGradedComponent } from './sounbath-graded/sounbath-graded.component';


const routes: Routes = [
    // { path: '', component: AppsComponent, outlet: 'app-content' },
  { path: 'apps', redirectTo: "/apps/(app-content:apps>test)", pathMatch: 'full' },
  { path: 'apps', component: AppsComponent, children: [
        // { path: '', redirectTo: 'apps>get-the-cake', pathMatch: 'full' },
        { path: 'apps>test', component: TestComponent, outlet: 'app-content' },
        { path: 'apps>audio-driven-shader', component: AudioDrivenShaderComponent, outlet: 'app-content' },
        { path: 'apps>soundbath', component: SoundbathComponent, outlet: 'app-content' },
        { path: 'apps>soundbath-graded', component: SounbathGradedComponent, outlet: 'app-content' },
        { path: 'games>get-the-cake', component: GetTheCakeComponent, outlet: 'app-content' },
        { path: 'games>an-all-hallows-tale', component: AnAllHallowsTaleComponent, outlet: 'app-content' }
      ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes),
        CommonModule],
    exports: [RouterModule],
    declarations: [
        AppListComponent,
        AppsComponent,
        TestComponent,
        AudioDrivenShaderComponent,
        GetTheCakeComponent,
        AnAllHallowsTaleComponent,
        SoundbathComponent,
        SounbathGradedComponent
    ]
})
export class AppsRoutingModule { }
