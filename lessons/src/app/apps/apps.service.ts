import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppsService {
  constructor() { }

  hidden: boolean = true;

    Toggle() {
        this.hidden = !this.hidden;

        var hamburger = document.querySelector(".hamburger");
        hamburger.classList.toggle("is-active");
    }
}
