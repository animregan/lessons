import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppsRoutingModule } from './apps-routing.module';
import { AppsService } from './apps.service';

@NgModule({
  imports: [
    CommonModule,
    AppsRoutingModule
  ],
  providers:[
      AppsService
  ],
  declarations: []
})
export class AppsModule { }
