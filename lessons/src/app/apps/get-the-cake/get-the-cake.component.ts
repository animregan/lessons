import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../globals.service';

@Component({
  selector: 'app-get-the-cake',
  templateUrl: './get-the-cake.component.html',
  styleUrls: ['./get-the-cake.component.css']
})
export class GetTheCakeComponent implements OnInit {
    constructor(private _globals: GlobalsService) {
        _globals.SetTitle("Get the Cake");
    }
    
    ngOnInit() {
    }
}
