import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../globals.service';

@Component({
    selector: 'app-an-all-hallows-tale',
    templateUrl: './an-all-hallows-tale.component.html',
    styleUrls: ['./an-all-hallows-tale.component.css']
})
export class AnAllHallowsTaleComponent implements OnInit {
    constructor(private _globals: GlobalsService) {
        _globals.SetTitle("An All Hallows Tale");
    }

    ngOnInit() {
    }

}
