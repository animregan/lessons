import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TutorialsService } from '../tutorials.service';
import { JsonService } from '../../json.service';

@Component({
    selector: 'app-tutorials-list',
    templateUrl: './tutorials-list.component.html',
    styleUrls: ['./tutorials-list.component.css']
})
export class TutorialsListComponent implements OnInit {
    constructor(private router: Router, _tutorialsService: TutorialsService, _jsonService: JsonService) {
        this.tutorialsService = _tutorialsService;

        _jsonService.readJson("assets/json/tutorials.json").subscribe(data => {
            this.jsonData = data;
        });
    }

    jsonData: string;
    tutorialsService: TutorialsService;

    ngOnInit() {
    }

    showTutorial(_id: string) {
        this.router.navigate(['/tutorials', { outlets: { 'tutorials-content': [_id] } }]);
        this.tutorialsService.Toggle();
    }
}
