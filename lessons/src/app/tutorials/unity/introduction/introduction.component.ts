import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.css']
})
export class IntroductionComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Introduction to unity");
  }

  ngOnInit() {
  }

}
