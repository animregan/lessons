import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-terrain',
  templateUrl: './terrain.component.html',
  styleUrls: ['./terrain.component.css']
})
export class TerrainComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Terrain");
  }

  ngOnInit() {
  }

}
