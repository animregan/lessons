import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-tracking-events',
  templateUrl: './tracking-events.component.html',
  styles: []
})
export class VuforiaTrackingEventsComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Tracking Events");
    }

  ngOnInit() {
  }

}
