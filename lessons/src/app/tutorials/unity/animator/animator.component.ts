import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-animator',
  templateUrl: './animator.component.html',
  styleUrls: ['./animator.component.css']
})
export class AnimatorComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Animator");
  }

  ngOnInit() {
  }

}
