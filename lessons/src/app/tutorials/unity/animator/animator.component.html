<p>
  If you followed the previous tutorial, you should have 2 AnimationClips:
</p>

<ul>
  <li>AmazingBoxIdle</li>
  <li>AmazingBoxWalk</li>
</ul>

<p>Why is it that we can see these clips in our clip list?</p>

<p>
  <img src="assets/images/unity/animator/clip-list.jpg">
</p>

<p>
  Well, remember that we create files when we create new AnimationClips. So for the 2 AnimationClips you see above, know that
  those actually refer to files which you can see in your project view:
</p>
<p>
  <img src="assets/images/unity/animator/2-anim-files.jpg">
</p>
<p>
  Somewhere, there is a container that establishes some association between these AnimationClips. That container is also visible in the above image, it's name is <i>AmazingBox </i>and it's what we call an AnimatorController. I can think of no better way to describe AnimatorControllers other than to say that they are containers that hold and manage multiple AnimationClips.
</p>

<p>
  This tutorial will aim to explain how we work with multiple animation clips on single objects.
</p>

<ol>
  <li>
    <p>
      Double click your AnimatorController as seen in the above image. It should open to the Animator window that looks like this:
    </p>
    <p>
      <img src="assets/images/unity/animator/animator.jpg">
    </p>
    <p>
      This might seem confusing at first, but there are actually 2 '<b>states</b>' here that we recognize: '<i>AmazingBoxIdle</i>' and '<i>AmazingBoxWalk</i>'. These are called states, and if you click on them, you will see in the inspector that they have those AnimationClip files assigned to them. You can rename these states if you don't want them named the same as the AnimationClips they represent, I see no need to do that, though.
    </p>
    <p>
      So what IS this graph? It's your AnimatorController. It holds references to your AnimationClips, This is why I call it a container. Why do we want our clips to be in a controller? Well, to help us organize them, for one, but more than that, this graph helps us make transitions.
    </p>
  </li>
  <li>
    <p>
      First thing's first though, what does this graph have to do with our <i>AmazingBox</i> ? How is it that this has anything to do with what animations the box uses? Well, if you click on ' <i>AmazingBox'</i> in the scene view, you will find this Animator component in the inspector:
    </p>
    <p>
      <img src="assets/images/unity/animator/animator-component.jpg">
    </p>
    <p>
      Notice that the AnimatorController we just described has been plugged in to this Animator component. The Animator component is nothing without this, it needs an AnimatorController to tell it which animations are available to it. You can double-click the AnimatorController you see here and it will instantly open up again to the Animator window, which is a handy shortcut.
    </p>
  </li>
  <li>
    <p>
      Now in the Animator window, start by right-clicking on your 'idle' state. You will see the option to make a transition, simply move your mouse over-top of the 'walk' state and click to drop the transition there. You will see a connection like below.
    </p>
    <p>
      <img src="assets/images/unity/animator/transition1.jpg">
    </p>
    <p>
      If you run your game and observe this graph, you should see a blue line run along your states to demonstrate where they are in transition. You should see your box animating accordingly in the scene and game views.
    </p>
    <p>
      The idea is that you can use this graph to map when a player character should be in idle state, walking, running, crouching, or jumping. Note that code is responsible for the motion, but to see arms and legs moving on a character, we need to be able to tell it which animations to play.
    </p>
  </li>
  <li>
    <p>
      We need to control when this transition is allowed to happen. So we will introduce parameters. Parameters are conditions that we can use to determine when a transition occurs. They can exist as decimal float values, whole integers, or boolean checkboxes. To create a parameter, find the '<i>parameters' </i>tab at the top-left of the Animator window. Within that tab, find the + symbol immediately below the tab and add a boolean named 'active'.
    </p>
    <p>
      <img src="assets/images/unity/animator/params.jpg">
    </p>
  </li>
  <li>
    <p>
      Great, there's a checkbox.... what do we do with it? You have a transition going from idle to walk, now right-click on walk, and make a transition back to idle.
    </p>
    <p>
      Fun fact: You can click on the transition lines, and observe their properties in the inspector:
    </p>
    <p>
      <img src="assets/images/unity/animator/transition-properties.jpg">
    </p>
    <p>
      For your interest, I have circled the transition lines so you know what to click on, and we can try to make some sense of the transition properties i've underlined in the inspector: <b>Has exit time: </b>Whether or not to wait until a certain point of the animation has been reached before being allowed to transition to the next animation. I typically leave this set to false unless i actively have a reason to leave it on.
    </p>
    <p>
      <b>Exit Time: </b>If you are using exit time, then we specify at which point the transition is allowed to happen. 0 means instantly, while 1 means it can only transition once the animation has finished. You can of course use other values like 0.3, 0.8 etc.
    </p>
    <p>
      <b>Transition Duration: </b>You may notice when you run your game that you can observe a blue line in this graph that represents which point the animation is currently at. If you increase transition duration, you will notice it takes longer to travel between idle and walk.
    </p>
  </li>
  <li>
    <p>
      I've also highlighted a little '<b>+</b>' symbol at the bottom of the above image. Hitting this button will add a '<b>condition</b>' <b> </b>to our transition. Remember how we made a parameter named 'active' in step 3? The reason we did that was to give us a checkbox to dictate whether or not our condition is allowed to happen or not. Hit the '<b>+</b>' button and it should automatically select the only condition we have, and you will have a condition that looks like this:
    </p>
    <p>
      <img src="assets/images/unity/animator/condition.jpg">
    </p>
    <p>
      Note that the condition says that this transition is only allowed to happen if the 'active' parameter is set to true. This should be the case for the transition from idle to walk. Make sure you have this set as such, and then do the reverse for the transition from walk to idle, that should only be allowed if active is true.
    </p>
    <p>
      I recommend disabling the exit time on both transitions for this exercize.
    </p>
  </li>
  <li>
    If you followed step 5, you should be able to run your game and in the Animator window, switch 'active' parameter (top-left) on/off to see your
    <i>AmazingBox</i> transitioning between the walk and idle states.
  </li>
</ol>

<p>
  As a side note, you often need some scripted mechanism to operate these parameters in-game. Here is a simple script you can apply to your
  <i>AmazingBox </i>that will automate the switching of your boolean for you.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">Animator</span> anim;

  <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Start</span>() &#123;
      anim <span style="color:#b4b4b4;">=</span> GetComponent&lt;<span style="color:#4ec9b0;">Animator</span>&gt;();
  
      <span style="color:#57a64a;">// Play this method after 5 seconds, then repeatedly, every 6 seconds</span>
      InvokeRepeating(<span style="color:#d69d85;">&quot;ToggleAnimator&quot;</span>, <span style="color:#b5cea8;">5.0f</span>, <span style="color:#b5cea8;">6.0f</span>);
  &#125;
  
  <span style="color:#569cd6;">void</span> ToggleAnimator() &#123;
      <span style="color:#57a64a;">// Set the active boolean of an animator controller to the opposite of its current state.</span>
      anim<span style="color:#b4b4b4;">.</span>SetBool(<span style="color:#d69d85;">&quot;active&quot;</span>, <span style="color:#b4b4b4;">!</span>anim<span style="color:#b4b4b4;">.</span>GetBool(<span style="color:#d69d85;">&quot;active&quot;</span>));
  &#125;

</pre>

<p>
  That's all there really is to it. To see this applied in a practical sense, I recommend importing the ThirdPersonCharacter via <i>Assets > Import > Character </i>and observing the script applied to the ThirdPersonCharacter prefab as well as opening up the AnimatorController in the Animator window. You will see that the script uses variables to simply operate in the first place, but it re-uses those variables to update the animator controller at the same time. This way, the scripted motion is also supplemented by animation in a very interconnected way. Hopefully after this and the previous tutorial, you understand now why I say that describing the Animation system in unity is not a linear discussion, and operates much more closely to the mechanical cog analogy i first described.
</p>