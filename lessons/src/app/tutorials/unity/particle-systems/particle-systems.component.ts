import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-particle-systems',
  templateUrl: './particle-systems.component.html',
  styleUrls: ['./particle-systems.component.css']
})
export class ParticleSystemsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Particle Systems");
  }

  ngOnInit() {
  }

}
