import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-client-rpc',
  templateUrl: './client-rpc.component.html',
  styleUrls: ['./client-rpc.component.css']
})
export class ClientRpcComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("ClientRPC");
    }

  ngOnInit() {
  }

}
