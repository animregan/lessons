import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-rigidbodies',
  templateUrl: './rigidbodies.component.html',
  styleUrls: ['./rigidbodies.component.css']
})
export class RigidbodiesComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Rigidbodies");
    }

  ngOnInit() {
  }

}
