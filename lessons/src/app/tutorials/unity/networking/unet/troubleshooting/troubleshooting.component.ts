import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-troubleshooting',
  templateUrl: './troubleshooting.component.html',
  styleUrls: ['./troubleshooting.component.css']
})
export class TroubleshootingComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Troubleshooting");
    }

  ngOnInit() {
  }

}
