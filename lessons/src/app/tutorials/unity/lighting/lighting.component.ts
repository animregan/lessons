import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-lighting',
  templateUrl: './lighting.component.html',
  styleUrls: ['./lighting.component.css']
})
export class LightingComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Lighting");
  }

  ngOnInit() {
  }

}
