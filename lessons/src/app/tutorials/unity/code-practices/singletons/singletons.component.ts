import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-singletons',
  templateUrl: './singletons.component.html',
  styleUrls: ['./singletons.component.css']
})
export class SingletonsComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Singletons");
    }

  ngOnInit() {
  }

}
