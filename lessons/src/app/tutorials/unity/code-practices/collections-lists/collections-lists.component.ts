import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-collections-lists',
  templateUrl: './collections-lists.component.html',
  styleUrls: ['./collections-lists.component.css']
})
export class CollectionsListsComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Collections: Lists");
    }

  ngOnInit() {
  }

}
