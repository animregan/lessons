import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-animation-curves',
  templateUrl: './animation-curves.component.html',
  styleUrls: ['./animation-curves.component.css']
})
export class AnimationCurvesComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Animation Curves");
    }

  ngOnInit() {
  }

}
