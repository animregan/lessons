import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-inverse-lerp',
  templateUrl: './inverse-lerp.component.html',
  styleUrls: ['./inverse-lerp.component.css']
})
export class InverseLerpComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Inverse Lerp");
    }

  ngOnInit() {
  }

}
