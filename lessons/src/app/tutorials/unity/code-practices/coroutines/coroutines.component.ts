import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-coroutines',
  templateUrl: './coroutines.component.html',
  styleUrls: ['./coroutines.component.css']
})
export class CoroutinesComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Coroutines");
    }

  ngOnInit() {
  }

}
