import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Events");
    }

    ngOnInit() {
    }

}
