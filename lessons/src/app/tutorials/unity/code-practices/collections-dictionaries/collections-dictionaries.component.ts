import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-collections-dictionaries',
  templateUrl: './collections-dictionaries.component.html',
  styleUrls: ['./collections-dictionaries.component.css']
})
export class CollectionsDictionariesComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Collections: Dictionaries");
    }

  ngOnInit() {
  }

}
