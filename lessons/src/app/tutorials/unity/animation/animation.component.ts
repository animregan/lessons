import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.css']
})
export class AnimationComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Animation");
  }

  ngOnInit() {
  }
}
