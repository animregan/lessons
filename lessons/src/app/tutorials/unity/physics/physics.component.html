<p>
  Physics are used for so many purposes in games. An obvious one is collisions, but it's also used to trigger events and to
  move characters and create arrays of effects.
</p>

<h2>The basics</h2>

<p>
  At it's most basic, a physics object must have a collider.
</p>

<p>
  <img src="assets/images/unity/physics/collider.jpg">
</p>

<p>
  This will allow a rigidbody to collide with it. If we want the object to respond to collisions, we need a collider AND a
  rigidbody.
</p>

<p>
  <img src="assets/images/unity/physics/collider-rigidbody.jpg">
</p>

<p>
  This is enough to achieve an invisible box that can be smashed around a scene. If we didn't want it to be invisible, we'd
  need to add A renderer, either to render a sprite or a mesh, note that MeshRenderers also require a MeshFilter.
</p>

<p>
  <img src="assets/images/unity/physics/meshfilter-renderer-collider-rigidbody.jpg">
</p>

<p>This is the raw essentials to achieve the following effect.</p>

<p>
  <img src="assets/images/unity/physics/basic-physics-demo.gif">
</p>

<p>
  I'll be using this blue sphere as a rigidbody that I can control with my mouse, for it to collide and to ignore gravity while
  I'm trying to drag it around, I need to enable
  <i>IsKinematic </i>on it's Rigidbody component.</p>
<p>
  <img src="assets/images/unity/physics/is-kinematic.jpg">
</p>

<p>
  Don't enable this for objects you want to have responding to gravity and collisions.
</p>

<h2>A mechanical use, introduction to layers</h2>

<p>
  Games tend to have rules, these vary immensely between games, let's say we have some bizarre rule that cubes can't collider
  with spheres (excluding our character) and vise-versa. For something like this, we need a way to tell physics objects to
  ignore other specific objects. The way to do this is with the use of layers.
</p>

<ol>
  <li>
    <p>
      At the top-right of your Unity window, you'll see a layers drop-down, click that and select
      <i>Edit Layers...</i>
    </p>
    <p>
      <img src="assets/images/unity/physics/layers.jpg">
    </p>
  </li>
  <li>
    <p>
      Input 2 new layers in any blank field of your choice. One will be called
      <i>Spheres </i>and one will be called
      <i>Cubes.</i>
    </p>
    <p>
      <img src="assets/images/unity/physics/edit-layers.jpg">
    </p>
  </li>
  <li>
    <p>
      Now select all of your spheres, and set them to their respective layer. Do the same for your cubes.
    </p>
    <p>
      <img src="assets/images/unity/physics/set-layers.gif">
    </p>
  </li>
  <li>
    <p>
      Now to make these two layers ignore each other, we need to change some settings. Browse to
      <i>Edit > ProjectSettings > Physics </i>and pay attention to the graph that appears, we simply want to break any association
      between the
      <i>Spheres</i> and
      <i>Cubes</i> layers.
    </p>
    <p>
      <img src="assets/images/unity/physics/layers-association.jpg">
    </p>
  </li>
  <li>
    <p>
      Job done, cubes will ignore spheres now. Remember that in my example, my blue sphere is not included in the Spheres layer.
    </p>
    <p>
      <img src="assets/images/unity/physics/ignore.gif">
    </p>
  </li>
</ol>
<h2>Joints</h2>
<p>
  This is not like the joints you may know from animation practices with regards to rigging. Joints in this case refer to connections
  between 2 Rigidbodies. You can use them to create physical effects or behaviours that simulate certain scenarios in real-life.
</p>
<p>
  <img src="assets/images/unity/physics/joints.gif">
</p>

<ol>
  <li>
    Simply select 1 of 2 Rigidbodies in your scene,
    <i>AddComponent </i>and search for
    <i>joint, </i>a list of joints will appear, ignore the 2D ones, those only work on 2D sprite objects.
  </li>
  <li>
    <p>
      Add a joint of your choice, such as a hinge or spring joint.
    </p>
    <p>
      <img src="assets/images/unity/physics/hinge-joint.jpg">
    </p>
  </li>
  <li>
    Drag some other Rigidbody into the
    <i>ConnectedBody</i> property, the two are now connected.
  </li>
  <li>
    If you want to be able to drag/drop one of these objects, you will need to enable
    <i>IsKinematic </i>on the drag-able object's rigidbody. Failure to do this will give you a pretty unfulfilling and potentially
    scary result.
  </li>
</ol>

<h2>
  Characters
</h2>

<p>
  Take the following situation where the blue capsule is a character.
</p>

<p>
  <img src="assets/images/unity/physics/situation.jpg">
</p>

<p>Let's look at some rudimentary form of motion in a video game. What are we trying to do? Simply get a character to move based
  on user input. With a little bit of code, that's easy.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">
  
  <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">float</span> speed <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">3.0f</span>;

  <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Update</span>() &#123;
    transform<span style="color:#b4b4b4;">.</span>position <span style="color:#b4b4b4;">+=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">Vector3</span>(<span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetAxis(<span style="color:#d69d85;">&quot;Horizontal&quot;</span>), <span style="color:#b5cea8;">0</span>, <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetAxis(<span style="color:#d69d85;">&quot;Vertical&quot;</span>)) <span style="color:#b4b4b4;">*</span> speed <span style="color:#b4b4b4;">*</span> <span style="color:#4ec9b0;">Time</span><span style="color:#b4b4b4;">.</span>deltaTime;
  &#125;
  
</pre>

<p>
  Observe that we get motion, but it's pretty useless as it simply translates this object but does not really collide nicely
  with rigidbodies, and ignores static colliders.
</p>

<p>
  <img src="assets/images/unity/physics/rudimentary-motion.gif">
</p>

<p>
  To adjust our code to collide with objects could be quite a phenomenal task. We'd have to have code that checks for objects
  nearby and only accept inputs if certain criteria are met, preventing us from moving through walls. We'd need code that
  detects movable objects to apply a force to knock them out of the way, and things get really complicated if we wanted to
  add jumping. This could mean a much larger script and a lot of future problems as we add features.
</p>

<p>
  A better alternative is to use a physics based approach, we attach a <i>Rigidbody </i>and apply the following script instead.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

  <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">float</span> speed <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">1.0f</span>;

  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">Rigidbody</span> rb;
  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">Vector2</span> input;
  
  <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Start</span>() &#123;
      <span style="color:#57a64a;">// Get a reference to our Rigidbody component.</span>
      rb <span style="color:#b4b4b4;">=</span> GetComponent&lt;<span style="color:#4ec9b0;">Rigidbody</span>&gt;();
  &#125;
  
  <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Update</span>() &#123;
      <span style="color:#57a64a;">// Receive input every frame.</span>
      input <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">Vector2</span>(<span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetAxis(<span style="color:#d69d85;">&quot;Horizontal&quot;</span>), <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetAxis(<span style="color:#d69d85;">&quot;Vertical&quot;</span>));
  &#125;
  
  <span style="color:#57a64a;">// Physics must be handled on a fixed update which ticks less frequently than our framerate.</span>
  <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">FixedUpdate</span>() &#123;
      <span style="color:#57a64a;">// Apply the value of our inputs to our velocity, leave the y velocity as it is.</span>
      rb<span style="color:#b4b4b4;">.</span>velocity <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">Vector3</span>(input<span style="color:#b4b4b4;">.</span>x <span style="color:#b4b4b4;">*</span> speed, rb<span style="color:#b4b4b4;">.</span>velocity<span style="color:#b4b4b4;">.</span>y, input<span style="color:#b4b4b4;">.</span>y <span style="color:#b4b4b4;">*</span> speed);
  &#125;

</pre>

<p>
  And, ta-da! Now that we're working with a physics object, our lives are made much easier. Collisions are handled by the engine,
  and the motion is only slightly more convoluted than the initial motion script.
</p>

<p>
  <img src="assets/images/unity/physics/bad-physics.gif">
</p>

<p>
  Ops, don't forget to freeze rotation on your
  <i>Rigidbody</i> component in the inspector.
</p>

<p>
  <img src="assets/images/unity/physics/constraints.jpg">
  <img src="assets/images/unity/physics/good-physics.gif">
</p>

<h2>Collision Detection / Triggers</h2>

<p>
  Another great use for the physics engine in Unity and in games in general is to use colliders as a means to trigger events.
  In that particular use-case, a collider is known as a
  <i>trigger box</i>. Note that the built-in physics engine is... NVidia's PhysX!
</p>

<p>
  Picture this, we want a sound or movie to play when our character reaches "the castle". Now without going into specifics,
  the first thing you need is the ability to activate some code when a character enters a certain area.
</p>

<ol>
  <li>Create a cube, set it's collider to IsTrigger. It will no longer block your character from passing through, but it will
    deliver messages to any scripts that ask for it.
  </li>
  <li>
    <p>
      Position this cube somewhere accessible to your character.
    </p>
    <p>
      <img src="assets/images/unity/physics/trigger-positions.jpg">
    </p>
  </li>
  <li>
    <p>
      Apply a script with the following code:
    </p>
    <p>
    </p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">
  
  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">Material</span> mat;

  <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Start</span>() &#123;
      mat <span style="color:#b4b4b4;">=</span> GetComponent&lt;<span style="color:#4ec9b0;">Renderer</span>&gt;()<span style="color:#b4b4b4;">.</span>material;
  &#125;
  
  <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">OnTriggerEnter</span>(<span style="color:#4ec9b0;">Collider</span> other) &#123;
      mat<span style="color:#b4b4b4;">.</span>color <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Color</span><span style="color:#b4b4b4;">.</span>black;
  &#125;
  
  <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">OnTriggerExit</span>(<span style="color:#4ec9b0;">Collider</span> other) &#123;
      mat<span style="color:#b4b4b4;">.</span>color <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Color</span><span style="color:#b4b4b4;">.</span>white;
  &#125;

</pre>
  </li>
  <li>
    <p>
      Observe the following result:
    </p>
    <p>
      <img src="assets/images/unity/physics/trigger-results.gif">
    </p>
  </li>
</ol>