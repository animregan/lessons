import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-physics',
  templateUrl: './physics.component.html',
  styleUrls: ['./physics.component.css']
})
export class PhysicsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Physics");
  }

  ngOnInit() {
  }

}
