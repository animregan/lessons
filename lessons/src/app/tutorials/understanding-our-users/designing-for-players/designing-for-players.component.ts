import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-designing-for-players',
  templateUrl: './designing-for-players.component.html',
  styleUrls: ['./designing-for-players.component.css']
})
export class DesigningForPlayersComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Designing for Players");
      }

  ngOnInit() {
  }
}
