import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-corporate-perspective',
  templateUrl: './corporate-perspective.component.html',
  styleUrls: ['./corporate-perspective.component.css']
})
export class CorporatePerspectiveComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Corporate Perspective");
      }

  ngOnInit() {
  }
}
