import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
    selector: 'app-ui-analysis',
    templateUrl: './ui-analysis.component.html',
    styleUrls: ['./ui-analysis.component.css']
})
export class UiAnalysisComponent implements OnInit {
    constructor(private globals: GlobalsService) {
        globals.SetTitle("UI Analysis");
    }

    ngOnInit() {
    }
}
