import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-the-strategy-game-learning-loop',
  templateUrl: './the-strategy-game-learning-loop.component.html',
  styleUrls: ['./the-strategy-game-learning-loop.component.css']
})
export class TheStrategyGameLearningLoopComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("The Strategy Game Learning Loop");
  }

  ngOnInit() {
  }

}
