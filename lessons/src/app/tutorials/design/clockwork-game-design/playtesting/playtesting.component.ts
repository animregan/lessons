import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-playtesting',
  templateUrl: './playtesting.component.html',
  styleUrls: ['./playtesting.component.css']
})
export class PlaytestingComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Playtesting");
  }

  ngOnInit() {
  }

}
