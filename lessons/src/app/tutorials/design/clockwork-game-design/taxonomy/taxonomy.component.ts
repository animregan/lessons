import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-taxonomy',
  templateUrl: './taxonomy.component.html',
  styleUrls: ['./taxonomy.component.css']
})
export class TaxonomyComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Taxonomy");
  }

  ngOnInit() {
  }
}
