import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-clockwork-game-design',
  templateUrl: './clockwork-game-design.component.html',
  styleUrls: ['./clockwork-game-design.component.css']
})
export class ClockworkGameDesignComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Clockwork Game Design");
  }

  ngOnInit() {
  }
}
