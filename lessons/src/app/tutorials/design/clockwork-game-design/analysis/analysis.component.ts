import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.css']
})
export class AnalysisComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Analysis");
  }

  ngOnInit() {
  }

}
