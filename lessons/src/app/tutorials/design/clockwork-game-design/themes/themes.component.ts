import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.css']
})
export class ThemesComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Themes");
  }

  ngOnInit() {
  }

}
