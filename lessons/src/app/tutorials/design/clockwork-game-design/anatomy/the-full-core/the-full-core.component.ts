import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-the-full-core',
  templateUrl: './the-full-core.component.html',
  styleUrls: ['./the-full-core.component.css']
})
export class TheFullCoreComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("The Full Core");
  }

  ngOnInit() {
  }

}
