import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-anatomy',
  templateUrl: './anatomy.component.html',
  styleUrls: ['./anatomy.component.css']
})
export class AnatomyComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Anatomy");
  }

  ngOnInit() {
  }

}
