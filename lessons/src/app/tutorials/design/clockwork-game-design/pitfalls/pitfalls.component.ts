import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-pitfalls',
  templateUrl: './pitfalls.component.html',
  styleUrls: ['./pitfalls.component.css']
})
export class PitfallsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Pitfalls");
  }

  ngOnInit() {
  }

}
