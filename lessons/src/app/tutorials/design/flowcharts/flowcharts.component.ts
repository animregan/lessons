import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-flowcharts',
  templateUrl: './flowcharts.component.html',
  styleUrls: ['./flowcharts.component.css']
})
export class FlowchartsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Flowcharts");
  }

  ngOnInit() {
  }

}
