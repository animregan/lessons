import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-level-design',
  templateUrl: './level-design.component.html',
  styleUrls: ['./level-design.component.css']
})
export class LevelDesignComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Level Design");
  }

  ngOnInit() {
  }
}
