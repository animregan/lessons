import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-branching',
  templateUrl: './branching.component.html',
  styleUrls: ['./branching.component.css']
})
export class GitTortoiseBranchingComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Branching");
  }

  ngOnInit() {
  }

}
