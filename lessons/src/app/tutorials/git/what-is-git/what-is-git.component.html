<p>
  Git is a <i>DVCS</i>, which depending on who you are talking to, sometimes means <i>Decentralized Version Control System </i> but sometimes means <i>Distributed Version Control System</i>, feel free to google this some more, but all you need to know to get started is that it's a place that stores files for you. Think of it like Dropbox or GoogleDrive on steroids. A great way to visualize a Git project is by watching the promotional video on <a href="http://gource.io/">gource.io</a>, it shows a very insightful visualization.
</p>

<h2>Why do I want to use Git?</h2>

<p>
  Sure, you can simply backup your files on Dropbox or GoogleDrive. But ask yourself a few questions:
</p>

<ul>
  <li>What if your project is quite complex?</li>
  <li>What if you're scared to break your project and need room to experiment?</li>
  <li>What if you don't want to change a file, because you know that Dropbox / GoogleDrive will instantly save over this file
    forever (with a questionable history system)?</li>
  <li>What if naming files like this "test_scene_lighting_03_final_4_final" is getting out of hand?</li>
  <li>What if you are working with other people, with a high risk of accidentally overwriting files? Or somebody doing a little
    'too much cleanup'?</li>
</ul>

<p>
  Well, version control alleviates ALL of these concerns and more, and Git happens to be a form of version control! It essentially gives you the ability to travel in time. You and your team-mates can do whatever you want with your work without destroying a project by losing or overwriting files. You can experiment on files as much as you like, and with the correct steps, you can simply revert the file if you decide you don't like it, or if your experiment yields bad results. Git is also a very secure way to keep your work, you have complete control over which files are / aren't allowed in your project. Not only do we keep our work safe from others, but we keep other people's work safe from ourselves, our derpy, human... selves. In saying that, we also aim to keep our own work safe from our own selves.
</p>

<h2>Who is Git for?</h2>

<p>
  Traditionally for software engineers, but as the world is heading further and further in to the digital age, artists can benefit a lot from it as well. This guide is written particularly for artists who are working on Game Development projects, where their work is very fragile and sensitive. The aim is to alleviate the risks, as software, including but not limited to games is risky business! A lot can go wrong, and in this day and age, artists need their work to be safe as well.
</p>

<h2>Terminology</h2>

<p>
  To demystify Git, it's important to understand the common terminology that you're likely to find across all the different applications of Git. Please continue to refer to this terminology guide as you continue to read through these guides. The basic terminology is as follows:
</p>

<ul>
  <li>remote repository</li>
  <li>local repository</li>
  <li>branch</li>
  <li>checkout / switch branch</li>
  <li>staging</li>
  <li>commit</li>
  <li>push</li>
  <li>pull</li>
</ul>

<p>
  There's more to it than this, much, much, much, much more. But to get a grasp on this, you can get pretty far by understanding these concepts alone. Let's take a look at each of them in more detail:
</p>

<ul>
  <li>
    <b>remote repository</b> - This is essentially the place where your project is held. Be it on a web server or on a local server. The remote is where you pull your work from, and push your work to. It holds all of your branches, and the files within those branches.
  </li>
  <li>
    <b>local repository</b> - This is a copy of the <i>remote repository </i>that everyone keeps on their computer. It only has the branches that you've personally chosen to checkout.
  </li>
  <li>
        
    <b>branch</b> <i>(see my guide: "<a [routerLink]="['/tutorials', {outlets: {'tutorials-content': ['git>what-are-branches']}}]">What are 'branches'?</a>")</i> - These are copies of your project that exist within your remote repository. You create branches, switch to them, and do
    work on them, then you merge them back to your development and master branches. 'master' is the default branch created
    for all Git repositories.
  </li>
  <li>
    <b>checkout / switch branch</b> - This is the process of switching to another branch. You <i>checkout</i> to that <i>branch</i>.
  </li>
  <li>
    <b>staging</b> - When you've finished working, you will have a lot of altered files. You need to commit them, or rather '<i>stage</i>' them for commit. <i>Un-staged</i> files will not be included when you commit your work, <i>staged</i> files will be.</li>
  <li>
    <b>commit</b> - This saves your work on your <i>local repository</i>. <i>Commit</i> is an essential step to being able to <i>push</i> your work to the <i>remote repository. </i>Your work MUST be <i>committed </i>in order to <i>push </i>it. <i>Committed </i>work will still be there if you leave the current branch to work on something else and then return back to this branch. A successful commit MUST consist of some staged files, and a message that identifies what changes are incurred by the commit.
  </li>
  <li>
    <b>push</b> - This is the step that takes all work <i>committed</i> on your <i>local repository</i>, and sends it to the <i>remote repository. </i>This is the final step to ensuring your work has been safely backed up.
  </li>
  <li>
    <b>pull</b> - Sometimes you have your work cloned to several computers, or you are collaborating in a team where everyone has <i>cloned </i>the <i>remote repository </i>on to their machine as a <i>local repository. </i>When someone <i>commits</i> and <i>pushes</i> their work and you are working on the same <i>branch</i> as them, the <i>remote </i>will be up-to-date with their latest work, but now your <i>local</i> will be outdated. To resolve this, you must <i>pull </i>their work from the <i>remote repository</i> to the <i>local repository</i> on your machine.</li>
</ul>

<p>
  I truly think it's a good idea not to get hung up on this stuff for now, as you are introducing yourself to Git. Learn this stuff naturally as you go. You can keep tabs on this page, and refer to it when you need to. Before we get started, I want to lightly introduce the concept of <a [routerLink]="['/tutorials', {outlets: {'tutorials-content': ['git>what-are-branches']}}]">branching</a>. It's a very big part of working with version control, particularly with Git. I have kept this quite small for now, as we'll touch on this in depth later as needed. Continue on to <a [routerLink]="['/tutorials', {outlets: {'tutorials-content': ['git>what-are-branches']}}]">branching</a>...
</p>