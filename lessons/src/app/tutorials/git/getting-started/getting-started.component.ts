import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-getting-started',
  templateUrl: './getting-started.component.html',
  styleUrls: ['./getting-started.component.css']
})
export class GitGettingStartedComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Getting Started");
  }

  ngOnInit() {
  }

}
