import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-git-introduction',
  templateUrl: './git-introduction.component.html',
  styleUrls: ['./git-introduction.component.css']
})
export class GitIntroductionComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Git Introduction");
  }

  ngOnInit() {
  }

}
