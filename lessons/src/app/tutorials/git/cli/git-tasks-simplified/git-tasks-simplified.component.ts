import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-git-tasks-simplified',
  templateUrl: './git-tasks-simplified.component.html',
  styleUrls: ['./git-tasks-simplified.component.css']
})
export class GitTasksSimplifiedComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Git Tasks Simplified");
  }

  ngOnInit() {
  }

}
