import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-common-git-commands',
  templateUrl: './common-git-commands.component.html',
  styleUrls: ['./common-git-commands.component.css']
})
export class CommonGitCommandsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Common Git Commands");
  }

  ngOnInit() {
  }

}
