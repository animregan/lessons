import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-kinaesthetics',
  templateUrl: './kinaesthetics.component.html',
  styleUrls: ['./kinaesthetics.component.css']
})
export class KinaestheticsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Kinaesthetics");
  }

  ngOnInit() {
  }
}
