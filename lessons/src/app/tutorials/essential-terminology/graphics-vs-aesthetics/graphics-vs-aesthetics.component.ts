import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-graphics-vs-aesthetics',
  templateUrl: './graphics-vs-aesthetics.component.html',
  styleUrls: ['./graphics-vs-aesthetics.component.css']
})
export class GraphicsVsAestheticsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Graphics Vs. Aesthetics");
  }

  ngOnInit() {
  }

}
