import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-complexity-not-equal-to-depth-or-realism',
  templateUrl: './complexity-not-equal-to-depth-or-realism.component.html',
  styleUrls: ['./complexity-not-equal-to-depth-or-realism.component.css']
})
export class ComplexityNotEqualToDepthOrRealismComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Complexity != Depth or Realism");
  }

  ngOnInit() {
  }

}
