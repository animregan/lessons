import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../globals.service';

@Component({
  selector: 'app-recent-history',
  templateUrl: './recent-history.component.html',
  styleUrls: ['./recent-history.component.css']
})
export class RecentHistoryComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Recent History");
  }

  ngOnInit() {
  }
}
