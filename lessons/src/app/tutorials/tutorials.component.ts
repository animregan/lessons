import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../globals.service';
import { TutorialsService } from './tutorials.service';

@Component({
    selector: 'app-tutorials',
    templateUrl: './tutorials.component.html',
    styleUrls: ['./tutorials.component.css']
})
export class TutorialsComponent implements OnInit {
    constructor(private _globals: GlobalsService, private _tutorialsService: TutorialsService) {
        this.globals = _globals;
        this.tutorialsService = _tutorialsService;
    }
    
    globals : GlobalsService;
    tutorialsService : TutorialsService;

    ngOnInit() {
        this.globals.SetPositionByNav();
        this.tutorialsService.hidden = true;
        // this.tutorialsService.Toggle();
    }
}
