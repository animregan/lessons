import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
    selector: 'app-facebook',
    templateUrl: './facebook.component.html',
    styleUrls: ['./facebook.component.css']
})
export class FacebookComponent implements OnInit {
    constructor(private globals: GlobalsService) {
        globals.SetTitle("Firebase Authentication - Facebook");
    }

    ngOnInit() {
    }

}
