import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-firebase-auth-getting-started',
  templateUrl: './firebase-auth-getting-started.component.html',
  styleUrls: ['./firebase-auth-getting-started.component.css']
})
export class FirebaseAuthGettingStartedComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Firebase Authentication - Getting Started");
    }

  ngOnInit() {
  }

}
