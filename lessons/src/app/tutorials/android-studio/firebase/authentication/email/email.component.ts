import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Firebase Authentication - Email");
    }

    ngOnInit() {
    }
}
