import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
    selector: 'app-basics',
    templateUrl: './basics.component.html',
    styleUrls: ['./basics.component.css']
})
export class BasicsComponent implements OnInit {
    constructor(private globals: GlobalsService) {
        globals.SetTitle("Firebase Database - Basics");
    }

    ngOnInit() {
    }

}
