import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
    selector: 'app-vuforia-world-builder-part-a',
    templateUrl: './vuforia-world-builder-part-a.component.html',
    styleUrls: ['./vuforia-world-builder-part-a.component.css']
})
export class VuforiaWorldBuilderPartAComponent implements OnInit {
    constructor(private globals: GlobalsService) {
        globals.SetTitle("Vuforia World Builder - Part 1");
    }

    ngOnInit() {
    }

}
