import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
    selector: 'app-vuforia-world-builder-part-c',
    templateUrl: './vuforia-world-builder-part-c.component.html',
    styleUrls: ['./vuforia-world-builder-part-c.component.css']
})
export class VuforiaWorldBuilderPartCComponent implements OnInit {
    constructor(private globals: GlobalsService) {
        globals.SetTitle("Vuforia World Builder - Part 3");
    }

    ngOnInit() {
    }
}
