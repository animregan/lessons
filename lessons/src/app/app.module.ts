import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { GlobalsService } from './globals.service';

import { AppComponent } from './app.component';

import { TutorialsModule } from './tutorials/tutorials.module';
import { AppsModule } from './apps/apps.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TutorialsModule,
    HttpClientModule,
    AppsModule
  ],
  providers: [
    Title,
    GlobalsService
  ],
  bootstrap: [AppComponent]
})
// @Injectable()
export class AppModule { 
}
