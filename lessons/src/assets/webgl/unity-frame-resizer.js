var gameContainer = document.getElementById("gameContainer");
var unityIFrame = window.parent.parent.document.getElementById("unity-iframe");

if (window.parent.parent != null) {            
    unityIFrame.style.maxWidth = gameContainer.offsetWidth + "px";
    console.log(unityIFrame.style.maxWidth);

    unityIFrame.setAttribute("width", gameContainer.offsetWidth);
    unityIFrame.setAttribute("height", gameContainer.offsetHeight);
    
    document.body.style.margin = "0px";

    window.addEventListener('resize', (() => {
        unityIFrame.setAttribute("height", unityIFrame.offsetWidth * 0.5625 + "px");
    }));
}