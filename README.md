# Installation

0. Make sure `npm` and `node.js` are installed.
0. `Angular CLI` must be installed, you can use the following line:
```
npm i -g @angular/cli
```
0. Serve the angular app on your localhost and open it automatically in a browser that watches for file-changes.
```
ng serve --open
```

# Workflow

## Adding new pages

Because this site is aiming to educate and follows the belief that topics are best categorized and taught within a stream of other relevant topics, this site is structured through the use of a particular hierarchy.

![](git-resources/component-hierarchy.jpg)

Here you can see a single component consisting of the following 3x files:
* `*.ts`
* `*.html`
* `*.css`

It's contained in its own `component` folder, which belongs to a `module` folder of the same name. It shares the module folder with other components that also belong under the same topic. If `goals` was broken into other topics, then the goals folder would have `goals.module.ts` and `goals-routing.module.ts` and have all of the topics belonging to it stored as their own sets of components in their own respective sub-folders. We may not necessarily find a `goals` component folder in the module folder, only if there happens to also be a page about goals. As far as the module itself is concerned, it's just a box for components to be placed into. If one of those happens to be a page about itself, so-be-it, but that page is a separate component housed in this module's folder.

In the above case, the routing module is imported in to the anatomy module. This is done automatically. So the anatomy module, above, was created with the following line:

```
ng g m lessons/design/clockwork-game-design/anatomy --routing
```

<hr>

After that, we can add components to this module with the following line:

```
ng g c lessons/design/clockwork-game-design/anatomy/goals
```

If we happened to have a topic to discuss about this module as well, we may also make a page/component for it.

```
ng g c lessons/design/clockwork-game-design/anatomy/anatomy
```

This will have imported and declared the new component within `anataomy.module.ts`, it's redundant to keep this here as we also have to repeat these imports and declarations in the `anatomy-routing.module.ts`. Just cut them from the module and paste them to their respectful places in the routing module.

<hr>

Note that the `anatomy` module will need to be imported in to the `clockwork-game-design` module, which itself is imported to the `design` module, which itself is imported to the `lessons` module. Seeing as `lessons` is a directory in the same level as the app itself, the `lessons` module has been imported into `app.module.ts`.

So you can see how modules allow us to work with a hierarchy with components being managed by our routing module, and our routing module being housed by a regular module, and that module is imported up the chain until we end up back at the app root.

All of the real work is handled by the routing modules, here's an example of the `anatomy-routing.module.ts` which manages 5 components.

``` ts
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnatomyComponent } from './anatomy/anatomy.component';
import { GoalsComponent } from './goals/goals.component';
import { SupportingMechanismsComponent } from './supporting-mechanisms/supporting-mechanisms.component';
import { TheCoreMechanismComponent } from './the-core-mechanism/the-core-mechanism.component';
import { TheFullCoreComponent } from './the-full-core/the-full-core.component';

const routes: Routes = [
  { path: 'lessons/design/clockwork-game-design/anatomy', component: AnatomyComponent },
  { path: 'lessons/design/clockwork-game-design/anatomy/goals', component: GoalsComponent },
  { path: 'lessons/design/clockwork-game-design/anatomy/supporting-mechanisms', component: SupportingMechanismsComponent },
  { path: 'lessons/design/clockwork-game-design/anatomy/the-core-mechanism', component: TheCoreMechanismComponent },
  { path: 'lessons/design/clockwork-game-design/anatomy/the-full-core', component: TheFullCoreComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [
    AnatomyComponent,
    GoalsComponent,
    SupportingMechanismsComponent,
    TheCoreMechanismComponent,
    TheFullCoreComponent
  ]
})
export class AnatomyRoutingModule { }
```

Now our HTML can create a link to any of these pages, for example:

``` html
<a routerLink='design/clockwork-game-design/anatomy/goals'>Goals</a>
```

## Adding code snippets

HTML documents can display pre-formatted code. There's an easy way to copy code as HTML exactly the way you need.

1. In `Visual Studio`, go to `Tools > Extensions & Updates` and from the `Online` section, you can search for an extension named "Copy as HTML".
1. Install this, you'll need to restart Visual Studio for it to work.
1. Now with any code selected in Visual Studio, you can go to `Edit > Copy as HTML` and you'll be able to paste valid HTML into any HTML document of your choice and it should appear very closely to the way it did in Visual Studio.
1. Because we're using `Angular`, trying to load a page with code in it will usually give us an error, we need to replace `{` and `}` with their respective escape characters: `&#123;` and `&#125;`
